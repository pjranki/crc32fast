#include "crc32fast.h"

namespace crc32fast {

	uint32_t calculate(const void *buf, uint32_t size, uint32_t crc)
	{
		uint8_t *p = (uint8_t*)buf;
		while (size--)
		crc = table[(crc ^ *p++) & 0xFF] ^ (crc >> 8);
		return crc ^ ~0U;
	}

}
