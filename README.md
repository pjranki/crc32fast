Fast CRC32
==========
Platform independent way to make fast crc32 calculations (uses a pre-computed table).

Language
--------
C++11 and up (should compile for anything, doesn't use any OS APIs).

Arduino
-------
Checkout/copy this library to your Arduino libraries folder.

How to use it
-------------
Include the library:

```cpp
#include <crc32fast.h>
```

Calculate CRC32:

```cpp
uint32_t value = crc32fast::calculate("Helloworld!", sizeof("Helloworld!"));
```

Calculate CRC32 (chaining):

```cpp
uint32_t value = crc32fast::calculate("Helloworld!", sizeof("Helloworld!"));
value = crc32fast::calculate("Bye!", sizeof("Bye!"), value);
```
